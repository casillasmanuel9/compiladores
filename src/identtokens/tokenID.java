/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identtokens;

import java.util.Arrays;

/**
 *
 * @author Juan Manuel Galindo Casillas, Brian Anibla Sobtres Guadarrama
 */
public class tokenID {
    /*Clase que guarda los caracteres del*/
    String minuscula = "abcdefghijklmnopqrstuvwxyz";
    String mayuscula = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String sepId = "!\t\n;{}(),; &|<>!=-+*/";
    String numeros = "0123456789";
    String numerosSinCero = "123456789";
    String llaves = "{}()";
    String aritmeticos = "-=+*/%";
    String logicos = "&|<>!=";
    String igual = "=";
    String dif = "!";
    String may = ">";
    String men = "<";
    String i = "i";
    String f = "f";
    String gionBajo = "_";
    String peso = "$";
    String espacio = " ";
    String r ="r";
    String t = "t";
    String u = "u";
    String n = "n";
    String e = "e";
    String l = "l";
    String s = "s";
    //String e = "e"; Ya esta definido
    String w = "w";
    String h = "h";
    String coma = ",";
    String puntoComa = ";";
    String escape = "\n\t";
    String otro = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\n\t";
    String diagonal="/";
    String asterisco = "*";
    String mas = "+";
    String menos = "menos";
    String and = "&";
    String or = "|";
    //String i = "i"; la i,e,l ya estan
    
    public boolean contiene(String cad,String tipo)
    {
        if(tipo.equals("minuscula"))
        {
            if(this.minuscula.contains(cad))
                return true;
        }else if(tipo.equals("mayuscula"))
        {
            if(this.mayuscula.contains(cad))
                return true;
        }else if(tipo.equals("separadorId"))
        {
            if(this.sepId.contains(cad))
                return true;
        }else if(tipo.equals("numeros"))
        {
            if(this.numeros.contains(cad))
                return true;
        }else if(tipo.equals("numerosSinCero"))
        {
            if(this.numerosSinCero.contains(cad))
                return true;
        }else if(tipo.equals("llaves"))
        {
            if(this.llaves.contains(cad))
                return true;
        }else if(tipo.equals("i"))
        {
            if(this.i.contains(cad))
                return true;
        }else if(tipo.equals("r"))
        {
            if(this.r.contains(cad))
                return true;
        }else if(tipo.equals("u"))
        {
            if(this.u.contains(cad))
                return true;
        }else if(tipo.equals("n"))
        {
            if(this.n.contains(cad))
                return true;
        }else if(tipo.equals("t"))
        {
            if(this.t.contains(cad))
                return true;
        }
        else if(tipo.equals("f"))
        {
            if(this.f.contains(cad))
                return true;
        }else if(tipo.equals("e"))
        {
            if(this.e.contains(cad))
                return true;
        }else if(tipo.equals("l"))
        {
            if(this.l.contains(cad))
                return true;
        }else if(tipo.equals("s"))
        {
            if(this.s.contains(cad))
                return true;
        }else if(tipo.equals("w"))
        {
            if(this.w.contains(cad))
                return true;
        }else if(tipo.equals("h"))
        {
            if(this.h.contains(cad))
                return true;
        }else if(tipo.equals("aritmetico")){
            if(this.aritmeticos.contains(cad))
                return true;
        }else if(tipo.equals("logicos")){
            if(this.logicos.contains(cad))
                return true;
        }else if(tipo.equals("igual")){
            if(this.igual.contains(cad))
                return true;
        }else if(tipo.equals("dif")){
            if(this.dif.contains(cad))
                return true;
        }else if(tipo.equals("may")){
            if(this.may.contains(cad))
                return true;
        }else if(tipo.equals("men")){
            if(this.men.contains(cad))
                return true;
        }else if(tipo.equals("guionBajo"))
        {
            if(this.gionBajo.contains(cad))
                return true;
        }else if(tipo.equals("peso"))
        {
            if(this.peso.contains(cad))
                return true;
        }else if(tipo.equals("espacio"))
        {
            if(this.espacio.contains(cad))
                return true;
        }else if(tipo.equals("coma"))
        {
            if(this.coma.contains(cad))
                return true;
        }else if(tipo.equals("parentesis") || tipo.equals("llaves"))
        {
            if(this.llaves.contains(cad))
                return true;
        }else if(tipo.equals("puntoComa"))
        {
            if(this.puntoComa.contains(cad))
                return true;
        }else if(tipo.equals("escape"))
        {
            if(this.escape.contains(cad))
                return true;
        }else if(tipo.equals("otro"))
        {
            if(this.otro.contains(cad))
                return true;
        }else if(tipo.equals("diagonal"))
        {
            if(this.diagonal.contains(cad))
                return true;
        }else if(tipo.equals("asterisco"))
        {
            if(this.asterisco.contains(cad))
                return true;
        }else if(tipo.equals("otro2"))
        {
            if(this.otro.contains(cad))
                return true;
        }else if(tipo.equals("mas"))
        {
            if(this.mas.contains(cad))
                return true;
        }else if(tipo.equals("menos"))
        {
            if(this.menos.contains(cad))
                return true;
        }else if(tipo.equals("and"))
        {
            if(this.and.contains(cad))
                return true;
        }else if(tipo.equals("or"))
        {
            if(this.or.contains(cad))
                return true;
        }
        return false;
    }
}
