package identtokens;

/**
 *
 * @author Juan Manuel Galindo Casillas, Brian Anibla Sobtres Guadarrama
 */
public class variablesVal extends tokenID {


    /*automara que valida variables*/
    String automataIDE[][]
            ={
                {"q0","guionBajo","q1"},
                {"q0","peso","q1"},
                {"q0","minuscula","q1"},
                {"q1","minuscula","q1"},
                {"q1","mayuscula","q1"},
                {"q1","numeros","q1"},
                {"q1","separadorId","qf"}
            };
    /*automara de palabras reservadas*/
    String automataPalRes[][]
            ={
                {"q0","i","q1"},
                {"q1","f","q3"},
                {"q1","n","q2"},
                {"q2","t","q3"},
                {"q0","e","q5"},
                {"q5","l","q6"},
                {"q6","s","q7"},
                {"q7","e","q3"},
                {"q0","w","q8"},
                {"q8","h","q9"},
                {"q9","i","q10"},
                {"q10","l","q11"},
                {"q11","e","q3"},
                {"q0","v","q12"},
                {"q12","o","q13"},
                {"q13","i","q14"},
                {"q14","d","q3"},
                {"q0","r","q15"},
                {"q15","e","q16"},
                {"q16","t","q17"},
                {"q17","u","q18"},
                {"q18","r","q19"},
                {"q19","n","q3"},
                {"q3","separadorId","qf"}
            };
    /*automara de numeros enteros*/
    String automataEntero[][]
            ={
                {"q0","numeros","q1"},
                {"q1","numeros","q1"},
                {"q1","separadorId","qf"}
            };
    /*automataSimbolosAritmeticos*/
    String automataSimbArit[][]
            ={
                {"q0","aritmetico","qf"},
            };
    /*automata para incrementos y decrementos*/
    String automataIncreDecre[][]
            ={
                {"q0","mas","q1"},
                {"q0","menos","q2"},
                {"q2","menos","qf"},
                {"q1","mas","qf"},
            };
    /*Automatas de operadores de relacion*/
    String automataComparacion[][]
            ={
                {"q0","men","q1"},
                {"q0","may","q1"},
                {"q0","dif","q1"},
                {"q0","igual","q1"},
                {"q1","igual","qf"},
                {"q1","otro","qf"},
            };
    /*Automatas para operadores logicos*/
    String automataLogicos[][]
            ={
                {"q0","dif","qf"},
                
                {"q0","and","q1"},
                {"q1","and","qf"},
                
                {"q0","or","q2"},
                {"q2","or","qf"}
            };
    /*Automatas para comas,parentesis,corchetes,punto y coma*/
    String automataOtro[][]
            ={
                {"q0","coma","qf"},
                {"q0","parentesis","qf"},
                {"q0","llaves","qf"},
                
                {"q0","puntoComa","qf"},
            };
    /*Automatas para Salidas de escape, espacios*/
    String caracterSpecial[][]
            ={
                {"q0","espacio","qf"},
                {"q0","escape","qf"}  
            };
    /*Automatas para los compentarios*/
    String comentariosMulti[][]
            ={
                {"q0","diagonal","q1"},
                {"q1","diagonal","q4"},
                {"q1","diagonal","qf"},
                {"q1","asterisco","q2"},
                {"q2","espacio","q2"},
                {"q2","otro2","q2"},
                {"q2","diagonal","q2"},
                {"q2","asterisco","q3"},
                {"q3","otro2","q2"},
                {"q3","asterisco","q3"},
                {"q3","diagonal","qf"},
                {"q4","espacio","q4"},
                {"q4","minuscula","q4"},
                {"q4","mayuscula","q4"},
                {"q4","numeros","q4"},
                {"q4","espacio","q4"},
                {"q4","otro2","qf"},
            };
    String automataOpCOmbinado[][]
            ={
                {"q0","aritmetico","q1"},
                {"q1","igual","qf"}
            };
    String edoIn = "q0";
    String edoFin = "qf";

    /*
    *   Funcion encargada de evaluar los automatas
    *       Retorna 0 si es aceptado, Retorna el apuntador al final de la aceptacion
    */
    public int validacion(String cadena,int apuntador,String automataTipo) {
        //System.out.println(apuntador);
        //System.out.println(cadena.charAt(apuntador));
        String[][] automata = this.automataTipo(automataTipo);
        String estadoAct = this.edoIn;
        int indEstado = 0;
        int j = apuntador;
        boolean otro = false;
        while (indEstado < automata.length && j<cadena.length()) {
            if (estadoAct.equals("qf")) {
                if(otro == true)
                {
                    return j-1;
                }
                return j;
            } else {
                if (automata[indEstado][0].equals(estadoAct)) 
                {
                    if (this.contiene(String.valueOf(cadena.charAt(j)),automata[indEstado][1])) 
                    {
                        if(automata[indEstado][1].equals("otro") || automata[indEstado][1].equals("separadorId"))
                        {
                            otro = true;
                        }
                        estadoAct = this.nuevoEstado(indEstado,automata);
                        indEstado = 0;//0
                        j++;                        
                    } else 
                    {
                        indEstado++;
                    }
                } else {
                    indEstado++;
                }
            }
        }
        if (estadoAct.equals("qf")) {
            if(otro == true)
                {
                    return j-1;
                }    
            return j;
        }
        return 0;
    }
    
    private String[][] automataTipo(String tipo)
    {
        if(tipo.equalsIgnoreCase("automataIDE")) 
            return this.automataIDE;
        if(tipo.equalsIgnoreCase("automataPalRes"))
            return this.automataPalRes;
        if(tipo.equalsIgnoreCase("automataEntero"))
            return this.automataEntero;
        if(tipo.equalsIgnoreCase("automataSimbAri"))
            return this.automataSimbArit;
        if(tipo.equalsIgnoreCase("automataComparacion"))
            return this.automataComparacion;
        if(tipo.equalsIgnoreCase("automataLogicos"))
            return this.automataLogicos;
        if(tipo.equalsIgnoreCase("automataOtro"))
            return this.automataOtro;
        if(tipo.equalsIgnoreCase("caracterSpecial"))
            return this.caracterSpecial;
        if(tipo.equalsIgnoreCase("comentariosMulti"))
            return this.comentariosMulti;
        if(tipo.equalsIgnoreCase("automataIncreDecre"))
            return this.automataIncreDecre;
        if(tipo.equalsIgnoreCase("automataOpCOmbinado"))
            return this.automataOpCOmbinado;
        return null;
    }
    
    private String nuevoEstado(int estado, String[][] automata) {
        return automata[estado][2];
    }
}
